# Summary

Source: https://gitlab.com/agrozyme-docker/node

Node.js is a JavaScript-based platform for server-side and networking applications.

# Running

When the container start running and has `package.json` in work directory, it will run `npm install`(optional) and `npm start`.

# Commands

- [npm](https://docs.npmjs.com/)

# Environment Variables

## NPM_CONFIG_CACHE

These variables are optional, default value is `/tmp/npm-cache`.

## NODE_NPM_INSTALL

These variables are optional, set `YES` to run `npm install` after boot.

## NODE_RUN

These variables are optional, set a path to run script.
