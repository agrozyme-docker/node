# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Node
      module Build
        def self.main
          System.run('apk add --no-cache nodejs-current npm')
          Shell.make_folders('/var/www/html')
        end
      end

      module Run
        def self.prepare
          if Paser.boolean(ENV['NODE_NPM_INSTALL'])
            System.run('npm install', sudo: USER)
          end
        end

        def self.main
          html = '/var/www/html'
          Shell.update_user
          Shell.change_owner(html)

          run = ENV.fetch('NODE_RUN', '')

          if '' != run
            System.execute('node', run, sudo: USER)
            return
          end

          if false == File.exist?("#{html}/package.json")
            return
          end

          System.invoke('Prepare', self.method(:prepare))
          System.execute('npm start', sudo: USER)
        end

      end
    end
  end
end
